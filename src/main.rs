mod lib;
use std::process::exit;
#[macro_use]
extern crate clap;
use clap::App;

/* Main function
 * load the yaml and get the matches
 * match the grabbed matches with strings
 * fire off the respective desired function
 */
fn main() {
    let yaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(yaml).get_matches();
    let cloudhandler = lib::CloudHandler;
    let connection = lib::CloudHandler::new(lib::CloudHandler);
    for i in &matches.args {
        match i.0 {
            &"list" => list_bays(matches
                            .value_of("list")
                            .unwrap()),
            &"list-all" => list_bays(
                            "list-all"),
            &"set" => set_bay(matches.values_of("set").unwrap()),
            &"clear" => clear_bay(matches
                            .value_of("clear")
                            .unwrap()),
            _        => println!("Default Case reached!"),
        }
    }
}

/* List bays
 * Takes: &str
 * Passes desired target to lib.rs
 * Queries database for the target and prints the results
 */
fn list_bays(_target: &str) {
    let cloudhandler = lib::CloudHandler;
    let connection = lib::CloudHandler::new(lib::CloudHandler);
    let _data = cloudhandler.read_db(connection, _target.to_string());
    print_bays(_data.unwrap());
}

/* set bay
 * Takes: &str
 * Passes desired target data
 * Sets the target in the database
 * Prints all bays
 */
fn set_bay(matches: clap::Values) {
    let cloudhandler = lib::CloudHandler;
    let connection = lib::CloudHandler::new(lib::CloudHandler);
    cloudhandler.write_db_entry(connection, matches).expect("Unable to set");
    list_bays("list-all");
}

/* Clear bay
 * Takes &str
 * Passes desired target data
 * Clears given bay
 * Prints all bays
 */
fn clear_bay(_target: &str) {
    let cloudhandler = lib::CloudHandler;
    let connection = lib::CloudHandler::new(lib::CloudHandler);
    cloudhandler.clear_db_entry(connection, _target.to_string()).expect("Unable to clear");
    list_bays("list-all");
}


/* Print Bays function
 * Takes: Vector<Struct> IE: [Cloubays { values }]
 * Check if the length of the return is populated
 * Else, print the contents of the Struct
 */
fn print_bays(bays: Vec<lib::CloudBays>) {
    if bays.len() < 1 {
        println!("That bay in unoccupied, set it?");
        exit(1);
    } else {
        for bay in bays {
            println!("Bay: {}\n => Request: \t{}\n => Task: \t{}\n => Size: \t{}",
                bay.id, bay.req_num, bay.task_num, bay.size);
        }
    }
}

