extern crate rusqlite;
use rusqlite::{Connection, NO_PARAMS};

#[derive(Debug)]
pub struct CloudHandler;

#[derive(Debug)]
pub struct CloudBays {
    pub id: i64,
    pub req_num: i64,
    pub task_num: i64,
    pub size: String,
}

impl CloudHandler {
    pub fn new(self) -> rusqlite::Connection {
        let path_to_db = "./lib/bays.db";
        let _c = rusqlite::Connection::open(path_to_db).unwrap();
        _c.execute(
            "CREATE TABLE IF NOT EXISTS bays (
                id          INTEGER PRIMARY KEY,
                req_num     INTEGER NOT NULL,
                task_num    INTEGER NOT NULL,
                size        TEXT NOT NULL
            )", NO_PARAMS,)
        .unwrap();
        return _c;
    }

    pub fn write_db_entry(self, connection: Connection, _data: clap::Values)
                                            -> Result <(), rusqlite::Error> {
        let mut temp = Vec::new();
        for i in _data {
            temp.push(i);
        }
        connection.execute("INSERT INTO bays
                (id, req_num, task_num, size) VALUES(?1,?2,?3,?4)",
                &[&temp[0], &temp[1], &temp[2], &temp[3]]);
        Ok(())
    }

    pub fn clear_db_entry(self, connection: Connection, _data: String)-> Result <(), rusqlite::Error> {
        println!("{}", _data);
        connection.execute("DELETE FROM bays WHERE id=(?1)", &[&_data]);
        Ok(())
    }

    pub fn read_db(self, connection: Connection, target: String)
                        -> Result <Vec<CloudBays>, rusqlite::Error> {
        let mut statement;
        if target != "list-all" {
            println!("{}", target);
            statement = connection
                .prepare("SELECT id,req_num,task_num,size FROM bays WHERE id = :id")
                .unwrap();
            let bay_iter = statement.query_map_named(&[(":id", &target)], |row| CloudBays {
                id: row.get(0),
                req_num: row.get(1),
                task_num: row.get(2),
                size: row.get(3),
            }).unwrap();
            let mut cbays = Vec::new();
            for entry in bay_iter {
                cbays.push(entry.unwrap())
            }
            cbays.reverse();
            Ok(cbays)
        } else {
            statement = connection
                .prepare("SELECT id, req_num, task_num, size FROM bays")
                .unwrap();
            let bay_iter = statement.query_map(NO_PARAMS, |row| CloudBays {
                    id: row.get(0),
                    req_num: row.get(1),
                    task_num: row.get(2),
                    size: row.get(3),
                }).unwrap();
            let mut cbays = Vec::new();
            for entry in bay_iter {
                cbays.push(entry.unwrap())
            }
            cbays.reverse();
            Ok(cbays)
        }
    }
}
